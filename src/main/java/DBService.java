package main.java;

import java.sql.*;
import java.util.ArrayList;

/**
 * Created by markeloff on 19.11.16.
 */
public class DBService {
    private static final String url = "jdbc:mysql://localhost:3306/VirtueDB";
    private static final String user = "root";
    private static final String password = "1548365";
    private static Connection connection;
    private static Statement statement;
    private static ResultSet resultSet;


    public ArrayList<Task> getListTasks() throws SQLException {
        try {
            // opening database connection to MySQL server
            connection = DriverManager.getConnection(url, user, password);

            // getting Statement object to execute query
            statement = connection.createStatement();

            // executing SELECT query
            resultSet = statement.executeQuery("SELECT * FROM help_request");
        } catch (SQLException e) {
            e.printStackTrace();
        }
        ArrayList<Task> taskList = new ArrayList<>();
        while (resultSet.next()) {
            Task task = new Task();
            task.setId(resultSet.getInt("id_request"));
            task.setDescription(resultSet.getString("description"));
            task.setLocation(resultSet.getString("location"));
            task.setUser(resultSet.getString("user"));
            taskList.add(task);
        }
        connection.close();
        statement.close();
        resultSet.close();
        return taskList;
    }

    public void addTask(Task task) throws SQLException {
        try {
            // opening database connection to MySQL server
            connection = DriverManager.getConnection(url, user, password);

            // getting Statement object to execute query
            statement = connection.createStatement();

            // executing SELECT query
            String phone=task.getUser();
            String description=task.getDescription();
            String location=task.getLocation();
            String query="INSERT INTO help_request VALUE (NULL,'"+phone+"','"+description+"','"+location+"')";
            System.out.println(query);
            int insertRow=statement.executeUpdate(query);
           // resultSet = statement.executeQuery(query);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        connection.close();
        statement.close();
    }


}
