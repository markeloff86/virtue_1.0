package main.java;

import main.keyboard.CustomKeyboard;
import org.telegram.telegrambots.TelegramApiException;
import org.telegram.telegrambots.TelegramBotsApi;
import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.objects.Message;
import org.telegram.telegrambots.api.objects.Update;
import org.telegram.telegrambots.api.objects.replykeyboard.buttons.KeyboardButton;
import org.telegram.telegrambots.api.objects.replykeyboard.buttons.KeyboardRow;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;

import java.sql.SQLException;
import java.util.ArrayList;

import static main.java.TextMessages.*;

public class VirtueBot extends TelegramLongPollingBot {
    Task newTask = new Task();

    public static void main(String[] args) {
        TelegramBotsApi telegramBotsApi = new TelegramBotsApi();
        try {
            telegramBotsApi.registerBot(new VirtueBot());
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String getBotUsername() {
        return "markeloff86";
    }

    @Override
    public String getBotToken() {
        return "293232770:AAGFOwBG-dKIhdPhJdUr1n5MYwnvwtvhSpc";
    }

    @Override
    public void onUpdateReceived(Update update) {
        Message message = update.getMessage();
        DBService dbService = new DBService();
        if (message != null) {
            if (message.hasText()) {
                if (message.getText().equals(START)) {
                    KeyboardRow keyboardRow = new KeyboardRow();
                    keyboardRow.add(new KeyboardButton(WANT_TO_HELP));
                    keyboardRow.add(new KeyboardButton(NEED_A_HELP));
                    sendMsg(message, START_MSG, keyboardRow);
                } else if (message.getText().equals(ACCEPT)) {
                    try {
                        if (newTask.getUser() != null && newTask.getLocation() != null && newTask.getDescription() != null) {
                            dbService.addTask(newTask);
                            newTask=new Task();
                        }
                        else sendMsg(message, FILL_ALL_FORMS);
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                } else if (message.getText().equals(NEED_A_HELP)) {
                    KeyboardRow keyboardRow = new KeyboardRow();
                    keyboardRow.add(new KeyboardButton(NEED_A_HELP_CONTACT).setRequestContact(true));
                    keyboardRow.add(new KeyboardButton(NEED_A_HELP_LOCATION).setRequestLocation(true));
                    keyboardRow.add(new KeyboardButton(DESCRIPTION));
                    keyboardRow.add(new KeyboardButton(ACCEPT));
                    sendMsg(message, FILL_ALL_FORMS, keyboardRow);
                } else if (message.getText().equals(WANT_TO_HELP)) {
                    ArrayList<Task> tasksList = new ArrayList<>();
                    try {
                        tasksList = dbService.getListTasks();
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                    for (Task task : tasksList)
                        sendMsg(message, task.toString());
                } else if (message.getText().equals(DESCRIPTION)) {
                    sendMsg(message, INPUT_DESC_OF_PROBLEM);
                } else {
                    newTask.setDescription(message.getText());
                }
            } else if (message.getContact() != null) {
                newTask.setUser(message.getContact().getPhoneNumber());
            } else if (message.hasLocation()) {
                newTask.setLocation(message.getLocation().getLatitude() + "," + message.getLocation().getLongitude());
            }
        }

    }


    private void sendMsg(Message message, String text) {
        SendMessage sendMessage = new SendMessage();
        sendMessage.enableMarkdown(true);
        sendMessage.setChatId(message.getChatId().toString());
        sendMessage.setReplyToMessageId(message.getMessageId());
        sendMessage.setText(text);
        try {
            sendMessage(sendMessage);
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }
    }


    private void sendMsg(Message message, String text, KeyboardRow keyboardRow) {
        SendMessage sendMessage = new SendMessage();
        sendMessage.enableMarkdown(true);
        sendMessage.setChatId(message.getChatId().toString());
        sendMessage.setReplyToMessageId(message.getMessageId());
        sendMessage.setText(text);
        new CustomKeyboard().clickCustomKeyboard(sendMessage, keyboardRow);
        try {
            sendMessage(sendMessage);
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }
    }
}