package main.java;

import org.telegram.telegrambots.api.objects.Location;
import org.telegram.telegrambots.api.objects.User;

/**
 * Created by markeloff on 19.11.16.
 */
public class Task {
    private int id;
    private String  user;
    private String description;
    private String location;

    public Task() {
    }

    public Task(String user, String location, String description) {

        this.user = user;
        this.location = location;
        this.description = description;
    }

    public int getId(){
        return this.id;
    }

    public void setId(int id){
        this.id=id;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    @Override
    public String toString() {
        return "Номер задачи: "+id+"\nНомер телефона нуждающегося: "+user+"\nОписание проблемы: "+description;
    }
}
