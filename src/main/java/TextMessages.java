package main.java;

/**
 * Created by markeloff on 19.11.16.
 */
public class TextMessages {
    public static final String ABOUT_MSG = "Привет, я Робот-Добродеятель!" +
            "\nЯ попробую решить твою проблему, главное доверься мне!" +
            "\nЧтобы начать работу со мной напиши /start";
    public static final String START = "/start";
    public static final String START_MSG = "Для продолжения работы выбрете то, что Вы хотели бы сделать: оказать помощь или просить помощи?";
    public static final String WANT_TO_HELP = "Хочу помочь!";
    public static final String NEED_A_HELP = "Нужна помощь!";
    public static final String NEED_A_HELP_LOCATION = "Передать местоположение для помощи";
    public static final String NEED_A_HELP_CONTACT = "Передать номер телефона для помощи";
    public static final String DESCRIPTION = "Описание";
    public static final String ACCEPT = "Готово";

    public static final String FILL_ALL_FORMS = "Заполните все пункты";
    public static final String INPUT_DESC_OF_PROBLEM = "Введите описание проблемы";
}
